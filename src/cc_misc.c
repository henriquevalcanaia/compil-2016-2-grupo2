#include "cc_misc.h"
#include "cc_dict.h"
#include "main.h"

extern struct comp_dict* hashTableSymbol;
extern int lines;

int comp_get_line_number (void)
{
  return lines;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s\n", mensagem); //altere para que apareça a linha
}

void main_init (int argc, char **argv)
{
  //implemente esta função com rotinas de inicialização, se necessário
	hashTableSymbol = dict_new();
}

void main_finalize (void)
{
  //implemente esta função com rotinas de inicialização, se necessário
}

void comp_print_table (void)
{
  int cont;
  if (hashTableSymbol == NULL) {
	printf("NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
  }
  for (cont = 0; cont < hashTableSymbol->size; cont++) {
      if (hashTableSymbol->data[cont]) {
      comp_dict_item_t* item;
      item = hashTableSymbol->data[cont];
      while (item != NULL) {
        int* v = item->content->line;
	int* type = item->content->type;
          cc_dict_etapa_2_print_entrada(item->key->lexeme, v, type);
          item = item->next;
        }
      }
    }
}
