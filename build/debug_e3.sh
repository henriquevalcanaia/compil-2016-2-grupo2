cmake -DETAPA_1=OFF -DETAPA_2=OFF -DETAPA_3=ON ..
make
./main tests/e3/i_e3_$1.ptg
dot e3.dot -Tpng -o saida.png
xdg-open saida.png

dot tests/e3/i_e3_$1.dot -Tpng -o tests/e3/saida_esperada.png >/dev/null
xdg-open tests/e3/saida_esperada.png
