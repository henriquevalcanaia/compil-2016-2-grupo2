/*
  Grupo 02
  Henrique Valcanaia
  Jonas Bohrer
  Renan Bortoluzzi
*/
%{
#include <stdio.h>
#include <stdlib.h>
#include "parser.h"  // arquivo gerado pelo bison

#include "cc_dict.h"
#include "main.h"
#include "cc_tree.h"

int lines = 1;
struct comp_dict* hashTableSymbol;

int* returnLine(int line);

%}

%x COMMENT

INT              (\-?)[0-9]+
FLOAT            (\-?)[0-9]*\.[0-9]+
CHAR             \'.\'
STRING           \"([^\"\\\n]|\\.)*\"					
ID               [a-zA-Z_][a-zA-Z0-9_]*
WHITESPACE       [[:space:]]
SPECIAL_CARACTER [\,\;\:\(\)\[\]\{\}\+\-\*\/\<\>\=\!\&\$\%\#\^]

%%

[ \t]+ { }
\n      { lines++; }
\/\/.*\n  { lines++; }

int          { return TK_PR_INT; 	}
float        { return TK_PR_FLOAT; 	}
bool         { return TK_PR_BOOL; 	}
char         { return TK_PR_CHAR; 	}
string       { return TK_PR_STRING;	}
if           { return TK_PR_IF; 	}
then         { return TK_PR_THEN; 	 }
else         { return TK_PR_ELSE; 	 }
while        { return TK_PR_WHILE;  }
do           { return TK_PR_DO; 	   }
input        { return TK_PR_INPUT; 	}
output       { return TK_PR_OUTPUT; 	}
return       { return TK_PR_RETURN; 	}                    
const        { return TK_PR_CONST; 	}
static       { return TK_PR_STATIC;	}
foreach      { return TK_PR_FOREACH; 	}
for          { return TK_PR_FOR; 	}
switch       { return TK_PR_SWITCH; 	}
case         { return TK_PR_CASE; 	}
break        { return TK_PR_BREAK; 	}
continue     { return TK_PR_CONTINUE; 	}
class        { return TK_PR_CLASS; 	}
private      { return TK_PR_PRIVATE; 	}
public       { return TK_PR_PUBLIC; 	}
protected    { return TK_PR_PROTECTED; 	}

"<="    { return TK_OC_LE; }
">="    { return TK_OC_GE; }
"=="    { return TK_OC_EQ; }
"!="    { return TK_OC_NE; }
"&&"    { return TK_OC_AND;}
"||"    { return TK_OC_OR; }
"<<"    { return TK_OC_SL; }
">>"    { return TK_OC_SR; }

true	{	
		comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
		comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
		itemKey->lexeme = strdup(yytext);	//strdup copia a string e aloca um espaço de memória
		itemKey->type = SIMBOLO_LITERAL_BOOL;
		itemContent->line = lines;
		itemContent->type = SIMBOLO_LITERAL_BOOL;
		itemContent->value.boolValue = atoi(yytext);
		yylval.valor_simbolo_lexico = dict_put(hashTableSymbol, itemKey, itemContent);
		return TK_LIT_TRUE;
	}

false                   {
		comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
		comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
		itemKey->lexeme = strdup(yytext);	//strdup copia a string e aloca um espaço de memória
		itemKey->type = SIMBOLO_LITERAL_BOOL;
		itemContent->line = lines;
		itemContent->type = SIMBOLO_LITERAL_BOOL;
		itemContent->value.boolValue = atoi(yytext);
		yylval.valor_simbolo_lexico = dict_put(hashTableSymbol, itemKey, itemContent);
		return TK_LIT_FALSE;
	}

{SPECIAL_CARACTER} {return yytext[0];}
 
{INT}	{	comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
		comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
		itemKey->lexeme = strdup(yytext);	//strdup copia a string e aloca um espaço de memória
		itemKey->type = SIMBOLO_LITERAL_INT;
		itemContent->line = lines;
		itemContent->type = SIMBOLO_LITERAL_INT;
		itemContent->value.intValue = atoi(yytext);
		yylval.valor_simbolo_lexico = dict_put(hashTableSymbol, itemKey, itemContent);
		return TK_LIT_INT;
	}
{FLOAT} {	comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
		comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
		itemKey->lexeme = strdup(yytext);	//strdup copia a string e aloca um espaço de memória
		itemKey->type = SIMBOLO_LITERAL_FLOAT;
		itemContent->line = lines;
		itemContent->type = SIMBOLO_LITERAL_FLOAT;
		itemContent->value.floatValue = atof(yytext);
		yylval.valor_simbolo_lexico = dict_put(hashTableSymbol, itemKey, itemContent);
		return TK_LIT_FLOAT;
	}
{ID}    {	comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
		comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
		itemKey->lexeme = strdup(yytext);	//strdup copia a string e aloca um espaço de memória
		itemKey->type = SIMBOLO_IDENTIFICADOR;
		itemContent->line = lines;
		itemContent->type = SIMBOLO_IDENTIFICADOR;
		itemContent->value.idValue = strdup(yytext);
		yylval.valor_simbolo_lexico = dict_put(hashTableSymbol, itemKey, itemContent);
		return TK_IDENTIFICADOR;
	}
{CHAR}	{	char c[1]; c[0] = yytext[1]; c[1] = 0;	//lembrar que yytext[0] = "
		comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
		comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
		itemKey->lexeme = strdup(c);	//strdup copia a string e aloca um espaço de memória
		itemKey->type = SIMBOLO_LITERAL_CHAR;
		itemContent->line = lines;
		itemContent->type = SIMBOLO_LITERAL_CHAR;
		itemContent->value.charValue = yytext[1];
		yylval.valor_simbolo_lexico = dict_put(hashTableSymbol, itemKey, itemContent);
		return TK_LIT_CHAR;
	}
{STRING} {	char s[yyleng]; strcpy(s, yytext+1); s[yyleng-2]='\0';
		comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
		comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
		itemKey->lexeme = strdup(s);	//strdup copia a string e aloca um espaço de memória
		itemKey->type = SIMBOLO_LITERAL_STRING;
		itemContent->line = lines;
		itemContent->type = SIMBOLO_LITERAL_STRING;
		itemContent->value.stringValue = strdup(s);
		yylval.valor_simbolo_lexico = dict_put(hashTableSymbol, itemKey, itemContent);
		return TK_LIT_STRING;
	}
		
"/*" BEGIN(COMMENT);
<COMMENT>{
     "*/"      BEGIN(INITIAL);
     [^*\n]+   
     "*"       
     \n        lines++;
}

.       return TOKEN_ERRO;

%%

int* returnLine(int line){
  int* pLine;
  pLine = (int *)malloc(sizeof(int));
  *pLine = line;
  return pLine;
}
